#include <math.h>
#include <functional>
#include "threadpool.h"
#include "controller.h"
//#include "nodeitem.h"

ThreadPool::ThreadPool(Controller* _parent, int _threadsCount):
    threadsCount(_threadsCount),
    parent(_parent),
    finished(false),
    threadsReady(0)
{
    for(int i = 0; i < threadsCount; i++)
    {
       threadsList.push_back(std::thread(&ThreadPool::workTask, this));
    }
}

ThreadPool::~ThreadPool()
{
    finished = true;

    threadsVar.notify_all();

    for(unsigned int i=0; i<threadsList.size(); i++)
        threadsList[i].join();
}


void ThreadPool::runCycle()
{
    std::unique_lock<std::mutex> _control_lock(controlLock);
//  if (threadsReady.load() < threadsCount)
//      controlVar.wait(_control_lock , [&](){ return threadsReady >= threadsCount; });

    threadsLock.lock(); //wait for all threads fall asleep
    threadsLock.unlock();

    threadsReady.store(0);

    threadsVar.notify_all();
    controlVar.wait(_control_lock);


}

map_iter ThreadPool::getBegin()
{
    //std::lock_guard<std::mutex> lck(listLock);
    return parent->guiMapping.begin();
}

map_iter ThreadPool::getEnd()
{
    //std::lock_guard<std::mutex> lck(listLock);
    return parent->guiMapping.end();
}


void ThreadPool::workTask()
{
    double r;



    std::function<double(double)> force([](double r)
    {
        return (1./r - 1./(r*r));

    });

    std::function<double(double, int, int)> grav([](double r, int m1, int m2)
    {
         return fabs(0.0667 *((m1-m2) / r*r));
    });

    std::unique_lock<std::mutex> _thread_lock(threadsLock);


    while(!finished)
    {
        _thread_lock.unlock();

        map_iter it = getBegin();

        while (it != getEnd())
        {
            bool expected = false;
            if (it->second->processedFlag.compare_exchange_strong(expected, true))
            {
                double x = it->second->getX();
                double y = it->second->getY();

                double forceX = 0 , forceY = 0;

                map_iter _it = getBegin();
                while(_it != getEnd())
                {
                    if (it != _it)
                    {
                        double _x = _it->second->getX();
                        double _y = _it->second->getY();

                        r = sqrt((x - _x) * (x - _x) + (y - _y) * (y - _y) );

                        forceX += force(r)*((_x - x)/r);
                        forceY += force(r)*((_y - y)/r);

                        //forceX += grav(r,it->second->mass, _it->second->mass)*((_x - x)/r);
                        //forceY += grav(r,it->second->mass, _it->second->mass)*((_y - y)/r);


                    }

                    it->second->velocityX = it->second->velocityX + forceX / it->second->mass;
                    it->second->velocityY = it->second->velocityY + forceY / it->second->mass;

                    _it++;
                }

            }

            it++;
        }

        _thread_lock.lock();
        if (threadsReady.fetch_add(1) >= threadsCount - 1)
        {
            controlLock.lock(); //wait for controller fall asleep
            controlLock.unlock();

            controlVar.notify_all();
        }

        threadsVar.wait(_thread_lock);
    }
}

