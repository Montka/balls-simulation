#include <QThread>
#include <QMouseEvent>
#include <QColor>
#include "guiwidget.h"
#include "graphicitem.h"

GuiWidget::GuiWidget(QWidget *parent): QGraphicsView(parent)
{
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(-200, -200, 400, 400);
    setScene(scene);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    setMinimumSize(400, 400);
}

GuiWidget::~GuiWidget()
{
    foreach (QGraphicsItem *item, scene()->items())
    {
        scene()->removeItem(item);
        item->~QGraphicsItem();
        ::operator delete[] (item);
    }
}

void GuiWidget::acceptMove(GraphicItem* item, QPointF _pos)
{
    QPointF pos = QGraphicsView::mapFromScene(_pos);

    emit askMove(item, pos.x(), pos.y());
}

void GuiWidget::timerEvent(QTimerEvent * te)
{
    Q_UNUSED(te);
    // possible animations group start point
}

void GuiWidget::mousePressEvent(QMouseEvent *event)
{
    GraphicItem* item = static_cast<GraphicItem* >(itemAt(event->pos()));
    if(event->button() == Qt::RightButton)
        if(!item)
            emit askAdd(event->x(), event->y());
        else
            emit askDelete(item);
    else
        QGraphicsView::mousePressEvent(event);
}

void GuiWidget::moveItem(GraphicItem *item, int x, int y)
{
    if(scene()->mouseGrabberItem() != item)
    {
        QPointF pos = QGraphicsView::mapToScene(x, y);
        item->setPos(pos);
    }
}

void GuiWidget::deleteItem(GraphicItem* item)
{
    scene()->removeItem(item);
    item->~QGraphicsItem();
    ::operator delete[] (item);
}
void GuiWidget::addItem(GraphicItem* item, int x, int y, int hue)
{
    QColor color = QColor::fromHsl(hue, 255, 100);
    new (item) GraphicItem(this, color);
    scene()->addItem(item);
    QPointF pos = QGraphicsView::mapToScene(x, y);
    item->setPos(pos);
}

void GuiWidget::resizeEvent(QResizeEvent *event)
{
    emit resizeNotify(event->size().width(), event->size().height());
}

#ifndef QT_NO_WHEELEVENT
void GuiWidget::wheelEvent(QWheelEvent *event)
{
    GraphicItem* item = static_cast<GraphicItem* >(itemAt(event->pos()));
    if(item)
    {
        emit changeMass(item, event->delta()/15);
    }
}
#endif

void GuiWidget::lockGraphicItem(GraphicItem* item)
{
    emit lockItem(item);
}
void GuiWidget::releaseGraphicItem()
{
    emit releaseItem();
}

void GuiWidget::changeItemColor(GraphicItem* item, int hue)
{
    item->color = QColor::fromHsl(hue, 255, 100);
    item->update();
}
