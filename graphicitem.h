#ifndef GRAPHICITEM_H
#define GRAPHICITEM_H

#include <QGraphicsItem>
#include "guiwidget.h"

class GraphicItem : public QGraphicsItem
{
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
public:
    GraphicItem(GuiWidget* _wgt, QColor _clr);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QPainterPath shape() const override;

    QColor color;

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    GuiWidget* wgt;

};

#endif // GRAPHICITEM_H
