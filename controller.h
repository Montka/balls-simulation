#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QTimer>
#include <unordered_map>
#include <vector>
#include <utility>

#include "nodeitem.h"

class GraphicItem;
class ThreadPool;

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent, size_t _sizeX, size_t _sizeY);
    ~Controller();

    std::unordered_map <GraphicItem*, NodeItem* > guiMapping;
signals:
    void moveGraphicItem(GraphicItem*, int x, int y);
    void addGraphicItem(GraphicItem*, int x, int y, int hue);
    void deleteGraphicItem(GraphicItem*);
    void setItemColor(GraphicItem*, int);

private:
    size_t sizeX;
    size_t sizeY;
    QTimer *timer;
    ThreadPool* pool;
    NodeItem* nodeHolded; //удерживаемый мышью шарик

public slots:
    void moveNode(GraphicItem*, int x, int y);
    void addNode(int x, int y);
    void deleteNode(GraphicItem* graphicItem);
    void resizeField(int x, int y);
    void lockNode(GraphicItem* item);
    void releaseNode();
    void changeNodeMass(GraphicItem* item, int delta);

private slots:
    void updateNodes();
};

#endif // CONTROLLER_H
