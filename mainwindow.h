#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include "guiwidget.h"
class Controller;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QThread thread;
    Controller* controller;
    GuiWidget widget;
};

#endif // MAINWINDOW_H
