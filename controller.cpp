#include <QDateTime>
#include <QThread>
#include <utility>
#include <atomic>
#include <functional>
#include <math.h>
#include <iostream>

#include "threadpool.h"
#include "graphicitem.h"
#include "controller.h"


Controller::Controller(QObject *parent, size_t _sizeX, size_t _sizeY) :
    QObject(parent),
    sizeX(_sizeX),
    sizeY(_sizeY)
{
    qsrand (QDateTime::currentMSecsSinceEpoch());

    pool = new ThreadPool(this, 2);

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateNodes()), Qt::ConnectionType::QueuedConnection);

    timer->start(10);
}

Controller::~Controller()
{
    delete timer;

    delete pool;

    for (auto it : guiMapping)
    {
        delete it.second;
    }
}

void Controller::updateNodes()
{
    pool->runCycle();
    for (auto it : guiMapping)
    {
        if (it.second == nodeHolded) continue;
        it.second->coordX = it.second->coordX + it.second->velocityX*10;
        it.second->coordY = it.second->coordY + it.second->velocityY*10;

        if(it.second->coordX < 0 || it.second->coordX > sizeX )
            it.second -> velocityX = - qMin(qMax(it.second->velocityX, -10.) , 10.);
        if(it.second->coordY < 0 || it.second->coordY > sizeY)
            it.second -> velocityY = - qMin(qMax(it.second->velocityY, -10.) , 10.);

        it.second->coordX = qMin(qMax(it.second->coordX, 0.) , (double)sizeX);
        it.second->coordY = qMin(qMax(it.second->coordY, 0.) , (double)sizeY);

        it.second->processedFlag = false;
        emit moveGraphicItem(it.second->graphicItem, round(it.second->coordX), round(it.second->coordY));
    }
}

void Controller::addNode(int x, int y)
{
    int mass = qrand() % 1000;
    NodeItem* node = new NodeItem(x, y, mass);
    guiMapping.insert(std::pair<GraphicItem*, NodeItem*> ( node->graphicItem, node));
    emit addGraphicItem(node->graphicItem, x, y, mass*(255./1000.));
}
void Controller::deleteNode(GraphicItem* graphicItem)
{
    auto iter = guiMapping.find(graphicItem);
    if (iter != guiMapping.end())
    {
       delete iter->second;
       guiMapping.erase(iter);
       emit deleteGraphicItem(graphicItem);
    }
}

void Controller::moveNode(GraphicItem* graphicItem, int x, int y)
{
    auto iter = guiMapping.find(graphicItem);
    if (iter != guiMapping.end())
    {
        iter -> second -> velocityX = (x - iter -> second -> coordX) * 0.05;
        iter -> second -> velocityY = (y - iter -> second -> coordY) * 0.05;
        iter -> second -> coordX = x;
        iter -> second -> coordY = y;

    }
}

void Controller::resizeField(int x, int y)
{
    sizeX = x;
    sizeY = y;
}

void Controller::lockNode(GraphicItem* item)
{
    nodeHolded = guiMapping.find(item)->second;
    nodeHolded->velocityX = 0;
    nodeHolded->velocityY = 0;
}
void Controller::releaseNode()
{
    nodeHolded = nullptr;
}

 void Controller::changeNodeMass(GraphicItem* item, int delta)
 {
     auto iter = guiMapping.find(item);
     if (iter != guiMapping.end())
     {

         iter->second->mass += delta*10;
         iter->second->mass = qMin(qMax(iter->second->mass, 10) , 1000);

         emit setItemColor(item, iter->second->mass *(255. / 1000.));

     }

 }
