#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <mutex>
#include <condition_variable>
#include <thread>
#include <atomic>
#include <vector>
#include <unordered_map>

#include "nodeitem.h"
class GraphicItem;
class Controller;

typedef std::unordered_map<GraphicItem*, NodeItem*>::iterator map_iter;

class ThreadPool
{

public:
    ThreadPool(Controller* _parent, int _threadsCount);
    ~ThreadPool();

    void runCycle();

private:

    int threadsCount;

    Controller* parent;
    std::vector<std::thread> threadsList;

    std::mutex controlLock;
    std::condition_variable controlVar;

    std::mutex threadsLock;
    std::condition_variable threadsVar;

    std::atomic<bool> finished;
    std::atomic<int> threadsReady;



    void workTask();

    map_iter getBegin();
    map_iter getEnd();
    std::mutex listLock;

};

#endif // THREADPOOL_H
