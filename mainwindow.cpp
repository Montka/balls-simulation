#include "mainwindow.h"
#include "controller.h"
#include "guiwidget.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    controller = new Controller(nullptr, 400, 400);
    controller->moveToThread(&thread);

    connect(&widget, SIGNAL(askAdd(int,int)), controller, SLOT(addNode(int,int)), Qt::ConnectionType::QueuedConnection);
    connect(controller, SIGNAL(addGraphicItem(GraphicItem*,int,int, int)), &widget, SLOT(addItem(GraphicItem*,int,int,int)),Qt::ConnectionType::QueuedConnection);

    connect(&widget, SIGNAL(askDelete(GraphicItem*)), controller, SLOT(deleteNode(GraphicItem*)), Qt::ConnectionType::QueuedConnection);
    connect(controller, SIGNAL(deleteGraphicItem(GraphicItem*)), &widget, SLOT(deleteItem(GraphicItem*)), Qt::ConnectionType::QueuedConnection);

    connect(&widget, SIGNAL(askMove(GraphicItem*,int,int)), controller, SLOT(moveNode(GraphicItem*,int,int)), Qt::ConnectionType::QueuedConnection);
    connect(controller, SIGNAL(moveGraphicItem(GraphicItem*,int,int)), &widget, SLOT(moveItem(GraphicItem*,int,int)), Qt::ConnectionType::QueuedConnection);

    connect(&widget, SIGNAL(resizeNotify(int,int)), controller, SLOT(resizeField(int,int)), Qt::ConnectionType::QueuedConnection);

    connect(&widget, SIGNAL(lockItem(GraphicItem*)), controller, SLOT(lockNode(GraphicItem*)), Qt::ConnectionType::QueuedConnection);
    connect(&widget, SIGNAL(releaseItem()), controller, SLOT(releaseNode()), Qt::ConnectionType::QueuedConnection);

    connect(&widget, SIGNAL(changeMass(GraphicItem*,int)), controller, SLOT(changeNodeMass(GraphicItem*,int)), Qt::ConnectionType::QueuedConnection);
    connect(controller, SIGNAL(setItemColor(GraphicItem*,int)), &widget, SLOT(changeItemColor(GraphicItem*,int)), Qt::ConnectionType::QueuedConnection);

    widget.show();

    thread.start();
}

MainWindow::~MainWindow()
{
    thread.quit();
    thread.wait();
    delete controller;
}
