#ifndef NODEITEM_H
#define NODEITEM_H
#include <atomic>

class GraphicItem;

class NodeItem
{
    friend class Controller;
public:
    NodeItem(int x, int y, int _mass);

    GraphicItem* graphicItem;

    std::atomic<bool> processedFlag;

    double velocityX;
    double velocityY;

    double getX() const;
    double getY() const;

    int mass;

private:
    double coordX;
    double coordY;


};

#endif // NODEITEM_H
