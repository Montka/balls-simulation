#ifndef GUIWIDGET_H
#define GUIWIDGET_H

#include <QGraphicsView>

class GraphicItem;

class GuiWidget : public QGraphicsView
{
   Q_OBJECT
signals:
    void askMove(GraphicItem* item, int x, int y);
    void askAdd(int x, int y);
    void askDelete(GraphicItem* item);
    void resizeNotify(int x, int y);
    void lockItem(GraphicItem* item);
    void releaseItem();
    void changeMass(GraphicItem*, int delta);

public:
    GuiWidget(QWidget *parent = 0);
    ~GuiWidget();
    void acceptMove(GraphicItem* item, QPointF _pos);

    void lockGraphicItem(GraphicItem* item);
    void releaseGraphicItem();

public slots:
    void moveItem(GraphicItem* item, int x, int y);
    void deleteItem(GraphicItem* item);
    void addItem(GraphicItem*, int x, int y, int hue);
    void changeItemColor(GraphicItem* item, int hue);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void timerEvent(QTimerEvent * te);
    void resizeEvent(QResizeEvent *event);
    void wheelEvent(QWheelEvent *event);

};

#endif // GUIWIDGET_H
