#include "graphicitem.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>
#include <QThread>


GraphicItem::GraphicItem(GuiWidget* _wgt, QColor _clr):
    color(_clr),
    wgt(_wgt)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
}
QRectF GraphicItem::boundingRect() const
{
    qreal adjust = 2;
    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}

//! [9]
QPainterPath GraphicItem::shape() const
{
    QPainterPath path;
    path.addEllipse(-10, -10, 20, 20);
    return path;
}

void GraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
           QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setBrush(color);
    painter->drawEllipse(-10, -10, 20, 20);
}

QVariant GraphicItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        if (wgt->scene()->mouseGrabberItem() == this)
            wgt->acceptMove(this, pos());
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}

void GraphicItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    wgt->lockItem(this);
    update();
    QGraphicsItem::mousePressEvent(event);
}

void GraphicItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    wgt->releaseItem();
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}
