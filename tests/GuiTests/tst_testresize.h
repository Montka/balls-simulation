#include <QTest>
#include <QApplication>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, ResizeTest)
{
    EXPECT_CALL( receiver, resizeNotifySlot(200,200) );
    QResizeEvent event(QSize(200,200), QSize(400,400));
    QApplication::sendEvent(wgt.viewport(), &event);
}
