#include "guitestclass.h"

GuiTestClass::GuiTestClass()
{
        QObject::connect(&wgt, &GuiWidget::askAdd, &receiver, &MockReceiver::askAddSlot);
        QObject::connect(&wgt, &GuiWidget::askDelete, &receiver, &MockReceiver::askDeleteSlot);
        QObject::connect(&wgt, &GuiWidget::askMove, &receiver, &MockReceiver::askMoveSlot);
        QObject::connect(&wgt, &GuiWidget::resizeNotify, &receiver, &MockReceiver::resizeNotifySlot);
        QObject::connect(&wgt, &GuiWidget::lockItem, &receiver, &MockReceiver::lockItemSlot);
        QObject::connect(&wgt, &GuiWidget::releaseItem, &receiver, &MockReceiver::releaseItemSlot);
        QObject::connect(&wgt, &GuiWidget::changeMass, &receiver, &MockReceiver::changeMassSlot);
}
