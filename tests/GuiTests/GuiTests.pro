include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
QT += core gui widgets testlib

INCLUDEPATH += ../../
HEADERS +=     tst_testrightclick.h \
                ../../guiwidget.h \
                ../../graphicitem.h \
    guitestclass.h \
    tst_testrightclickitem.h \
    tst_testleftclickitem.h \
    tst_testresize.h \
    tst_testwheelevent.h \
    tst_testadditem.h \
    tst_testdeleteitem.h \
    tst_testmousemove.h \
    tst_testmoveitem.h

SOURCES +=     main.cpp \
               ../../guiwidget.cpp \
                ../../graphicitem.cpp \
    guitestclass.cpp
