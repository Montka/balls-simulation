#include <QTest>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, RightClickItemTest)
{
    GraphicItem* item = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    wgt.addItem(item, 10, 10, 100);
    EXPECT_CALL( receiver, askDeleteSlot(item) );
    QTest::mouseClick(wgt.viewport(), Qt::MouseButton::RightButton, Qt::NoModifier, QPoint(10,10));
}
