#include <QTest>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, LeftClickItemTest)
{
    GraphicItem* item = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    wgt.addItem(item, 10, 10, 100);
    EXPECT_CALL( receiver, lockItemSlot(item) );
    EXPECT_CALL(receiver, releaseItemSlot());
    QTest::mouseClick(wgt.viewport(), Qt::MouseButton::LeftButton, Qt::NoModifier, QPoint(10,10));
}
