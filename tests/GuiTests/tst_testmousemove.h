#include <QTest>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, MouseMoveTest)
{
    GraphicItem* item = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    wgt.addItem(item, 10, 10, 100);
    EXPECT_CALL( receiver, lockItemSlot(item) );
    //EXPECT_CALL(receiver, askMoveSlot(item, 20, 20 )); // Impossible to test. See QTBUG-60234
    EXPECT_CALL(receiver, releaseItemSlot());
    QTest::mousePress(wgt.viewport(), Qt::MouseButton::LeftButton, Qt::NoModifier, QPoint(10,10), 2);
    QTest::mouseMove(&wgt, QPoint(10,11), 2);
    QTest::mouseRelease(wgt.viewport(), Qt::MouseButton::LeftButton, Qt::NoModifier, QPoint(11,11));
}
