#include "tst_testrightclick.h"
#include "tst_testrightclickitem.h"
#include "tst_testleftclickitem.h"
#include "tst_testmousemove.h"
#include "tst_testresize.h"
#include "tst_testwheelevent.h"
#include "tst_testadditem.h"
#include "tst_testdeleteitem.h"
#include "tst_testmoveitem.h"
#include <QApplication>
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    QApplication a(argc, argv);
    return RUN_ALL_TESTS();
}
