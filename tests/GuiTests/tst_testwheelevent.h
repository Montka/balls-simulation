#include <QTest>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, WheelEventTest)
{
    GraphicItem* item = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    wgt.addItem(item, 10, 10, 100);
    EXPECT_CALL( receiver, changeMassSlot(item, 10) );
    QWheelEvent event(QPointF(10,10),
                      150,
                      Qt::MouseButton::NoButton,
                      Qt::NoModifier
                      );
    QApplication::sendEvent(wgt.viewport(), &event);
}
