#include <QTest>
#include <QApplication>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, DeleteItemTest)
{
    GraphicItem* item = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    wgt.addItem(item, 10, 10, 100);
    ASSERT_EQ(wgt.scene()->items().size(), 1);
    wgt.deleteItem(item);
    ASSERT_EQ(wgt.scene()->items().size(), 0);
}
