#ifndef GUITESTCLASS_H
#define GUITESTCLASS_H
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "gmock/gmock.h"

#include <QObject>
#include <QWidget>

#include "graphicitem.h"
#include "guiwidget.h"


class Receiver : public QObject
{
public:
    virtual void askAddSlot(int, int) = 0;
    virtual void askMoveSlot(GraphicItem* item, int x, int y) = 0;
    virtual void askDeleteSlot(GraphicItem* item) = 0;
    virtual void resizeNotifySlot(int x, int y) = 0;
    virtual void lockItemSlot(GraphicItem* item) = 0;
    virtual void releaseItemSlot() = 0;
    virtual void changeMassSlot(GraphicItem*, int delta) = 0;
};

class MockReceiver : public Receiver
{
public:
    MOCK_METHOD2(askAddSlot, void(int, int));
    MOCK_METHOD3(askMoveSlot, void(GraphicItem*, int, int));
    MOCK_METHOD1(askDeleteSlot, void(GraphicItem*));
    MOCK_METHOD2(resizeNotifySlot, void(int, int));
    MOCK_METHOD1(lockItemSlot, void(GraphicItem*));
    MOCK_METHOD0(releaseItemSlot, void());
    MOCK_METHOD2(changeMassSlot, void(GraphicItem*, int));

};


class GuiTestClass : public ::testing::Test
{

public:

    GuiTestClass();

    GuiWidget wgt;
    MockReceiver receiver;

};

#endif // GUITESTCLASS_H
