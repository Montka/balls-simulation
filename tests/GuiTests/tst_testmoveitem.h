#include <QTest>
#include <QApplication>
#include <QGraphicsView>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, MoveItemTest)
{
    GraphicItem* item = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    wgt.addItem(item, 10, 10, 100);
    QPointF point1 = wgt.mapToScene(10,10);
    QPointF point2 = wgt.mapToScene(20,20);
    ASSERT_EQ(wgt.scene()->items().size(), 1);
    ASSERT_EQ(item->pos().x(), point1.x());
    ASSERT_EQ(item->pos().y(), point1.y());
    wgt.moveItem(item, 20, 20);
    ASSERT_EQ(item->pos().x(), point2.x());
    ASSERT_EQ(item->pos().y(), point2.y());
}
