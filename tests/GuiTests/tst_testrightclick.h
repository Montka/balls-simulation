#include <QTest>
#include "guitestclass.h"

using namespace testing;

TEST_F(GuiTestClass, RightClickTest)
{
    EXPECT_CALL( receiver, askAddSlot(10,10) );
    QTest::mouseClick(wgt.viewport(), Qt::MouseButton::RightButton, Qt::NoModifier, QPoint(10,10));
}
