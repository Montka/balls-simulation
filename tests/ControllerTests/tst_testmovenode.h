#include "controllertestclass.h"

using namespace testing;

TEST_F(ControllerTestClass, MoveNodeTest)
{
    GraphicItem* item;
    EXPECT_CALL(receiver, addGraphicItemSlot(_, 10,10,_)).WillOnce(::testing::SaveArg<0>(&item));
    controller.addNode(10,10);
    ASSERT_EQ(controller.guiMapping.size(), 1);
    ASSERT_EQ(controller.guiMapping.begin()->second->getX(), 10);
    ASSERT_EQ(controller.guiMapping.begin()->second->getY(), 10);

    controller.moveNode(item, 20, 30);
    ASSERT_EQ(controller.guiMapping.begin()->second->getX(), 20);
    ASSERT_EQ(controller.guiMapping.begin()->second->getY(), 30);

}
