#ifndef CONTROLLERTESTCLASS_H
#define CONTROLLERTESTCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "gmock/gmock.h"

#include <QObject>

#include "controller.h"
#include "nodeitem.h"
#include "graphicitem.h"

class Receiver : public QObject
{
public:
    virtual void moveGraphicItemSlot(GraphicItem*, int x, int y)=0;
    virtual void addGraphicItemSlot(GraphicItem*, int x, int y, int hue)=0;
    virtual void deleteGraphicItemSlot(GraphicItem*)=0;
    virtual void setItemColorSlot(GraphicItem*, int)=0;

};

class MockReceiver : public Receiver
{
public:
    MOCK_METHOD3(moveGraphicItemSlot, void(GraphicItem*, int, int));
    MOCK_METHOD4(addGraphicItemSlot, void(GraphicItem*, int, int, int));
    MOCK_METHOD1(deleteGraphicItemSlot, void(GraphicItem*));
    MOCK_METHOD2(setItemColorSlot, void(GraphicItem*, int));

};


class ControllerTestClass : public ::testing::Test
{

public:

    ControllerTestClass();

    Controller controller;
    MockReceiver receiver;

};


#endif // CONTROLLERTESTCLASS_H
