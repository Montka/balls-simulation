include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
QT += core gui widgets testlib

INCLUDEPATH += ../../
HEADERS +=     ../../controller.h \
               ../../nodeitem.h \
               ../../threadpool.h \
               tst_testaddnode.h \
    controllertestclass.h \
    tst_testdeletenode.h \
    tst_testmovenode.h \
    tst_testchangemass.h

SOURCES +=     main.cpp \
               ../../controller.cpp \
               ../../nodeitem.cpp \
               ../../threadpool.cpp \
    controllertestclass.cpp
