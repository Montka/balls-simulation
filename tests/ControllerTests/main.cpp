#include "tst_testaddnode.h"
#include "tst_testdeletenode.h"
#include "tst_testmovenode.h"
#include "tst_testchangemass.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
