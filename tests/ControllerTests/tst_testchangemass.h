#include "controllertestclass.h"

using namespace testing;

TEST_F(ControllerTestClass, ChangeMassTest)
{
    GraphicItem* item;
    EXPECT_CALL(receiver, addGraphicItemSlot(_, 10,10,_)).WillOnce(::testing::SaveArg<0>(&item));
    EXPECT_CALL(receiver, setItemColorSlot(_, _));
    controller.addNode(10,10);
    ASSERT_EQ(controller.guiMapping.size(), 1);
    controller.changeNodeMass(item, 10);

}
