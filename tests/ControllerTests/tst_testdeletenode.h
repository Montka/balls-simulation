#include "controllertestclass.h"

using namespace testing;

TEST_F(ControllerTestClass, DeleteNodeTest)
{
    GraphicItem* item;
    EXPECT_CALL(receiver, addGraphicItemSlot(_, 10,10,_)).WillOnce(::testing::SaveArg<0>(&item));
    EXPECT_CALL(receiver, deleteGraphicItemSlot(_));
    controller.addNode(10,10);
    ASSERT_EQ(controller.guiMapping.size(), 1);
    controller.deleteNode(item);
    ASSERT_EQ(controller.guiMapping.size(), 0);
}
