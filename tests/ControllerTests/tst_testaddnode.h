#include "controllertestclass.h"

using namespace testing;

TEST_F(ControllerTestClass, AddNodeTest)
{
    EXPECT_CALL(receiver, addGraphicItemSlot(_, 10,10,_));
    controller.addNode(10,10);
    ASSERT_EQ(controller.guiMapping.size(), 1);
}
