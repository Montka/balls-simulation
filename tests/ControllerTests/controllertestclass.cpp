#include "controllertestclass.h"

ControllerTestClass::ControllerTestClass():
    controller(nullptr, 400, 400)
{
    QObject::connect(&controller, &Controller::moveGraphicItem, &receiver, &MockReceiver::moveGraphicItemSlot);
    QObject::connect(&controller, &Controller::addGraphicItem, &receiver, &MockReceiver::addGraphicItemSlot);
    QObject::connect(&controller, &Controller::deleteGraphicItem, &receiver, &MockReceiver::deleteGraphicItemSlot);
    QObject::connect(&controller, &Controller::setItemColor, &receiver, &MockReceiver::setItemColorSlot);
}
