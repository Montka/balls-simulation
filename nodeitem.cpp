#include "nodeitem.h"
#include "graphicitem.h"

NodeItem::NodeItem(int x, int y, int _mass):
    velocityX(0),
    velocityY(0),
    mass(_mass),
    coordX(x),
    coordY(y)

{
    graphicItem = static_cast<GraphicItem*>(operator new (sizeof(GraphicItem)));
    processedFlag = false;


}

double NodeItem::getX() const
{
    return coordX;
}
double NodeItem::getY() const
{
    return coordY;
}
